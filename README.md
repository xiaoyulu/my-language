# MyLanguage

简单易用的多语言扩展。此扩展适合自定义多语言。



安装

- PHP 7.4+

```shell
$ composer require lilei/my-language
```



语言文件结构如下，如果是想要自定义语言文件，请使用如下结构：

`src/language/default.php`

```php
return [
    'success'   => [
        'zh_cn' => '成功',
        'en'    => 'successful'
    ]
];
```



使用

```php
use LiLei\MyLanguage\MyLanguage;

$myLanguage = new MyLanguage();
$myLanguage->getMessage('success', 'zh_cn');// 成功
$myLanguage->getMessage('success', 'en');// successful
```



设置语言文件路径

```php
$path = dirname(__FILE__) . "/language.php";

$myLanguage = new MyLanguage($path);// 方式一

$myLanguage->setPath($path);// 方式二
```



自定义语言

```php
$myLanguage->customMessage('hello', 'zh_cn', '你好啊！');
$myLanguage->getMessage('hello', 'zh_cn');// 你好啊！
```

