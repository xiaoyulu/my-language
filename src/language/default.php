<?php

return [
    'success'   => [
        'zh_cn' => '成功',
        'en'    => 'successful'
    ],
    'failed'    => [
        'zh_cn' => '失败',
        'en'    => 'failed'
    ],

    // 正确提示
    's_login' => [
        'zh_cn' => '登录成功',
        'en'    => 'Login succeeded'
    ],

    // 错误提示
    'e_login'   => [
        'zh_cn' => '登录失败',
        'en'    => 'Login failed'
    ],
    'e_login_1' => [
        'zh_cn' => '请先登录',
        'en'    => 'Please login first'
    ],
    'e_account' => [
        'zh_cn' => '账户或密码错误',
        'en'    => 'Account or password error'
    ],
    'e_parameter'   => [
        'zh_cn' => '参数错误',
        'en'    => 'Parameter error'
    ],
    'e_upload_file' => [
        'zh_cn' => '文件上传失败',
        'en'    => 'File upload failed'
    ],
];